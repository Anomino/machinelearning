#!/usr/bin/env python3
# coding:UTF-8

import numpy as np
import pandas as pd

SAVE_PASS = '../data/'

# 上限値を受け取る。
# エラトステネスのふるいにかけ、得た素数をリストにして返す。
def GeneratePrimeNumbers(limit = 100) : 
    # limitまでの数列と、素数をリストに
    numbers = range(3, limit + 1)
    primes = pd.DataFrame([[1, 2]])
    index = 1

    for x in numbers : 
        for y in primes[1] : 
            #print("{0} / {1}".format(x, y))
            if (x % y) == 0 : 
                break

        else : 
            #print(x)
            index += 1
            tmp = pd.DataFrame([[index, x]])
            primes = primes.append(tmp)

    return primes


def SavePrimes(limit = 100, filename = "primes.csv") : 
    primes = GeneratePrimeNumbers(limit)

    primes.to_csv(SAVE_PASS + filename)

def Test() : 
    primes = GeneratePrimeNumbers(limit = 100)
    print(primes)


if __name__ == "__main__" : 
    Test()
    #SavePrimes(limit = 100)
