#coding:UTF-8
import numpy as np
import matplotlib.pyplot as plt

# 多項式を最小二乗法と最急降下法を用いて、多項式回帰する。
# -最小二乗法-
# 1. 独立変数と任意の多項式を設定。
# 2. 次元を任意で設定し、設定した次元の重みベクトルをランダムでセットする。
# 3. 独立変数をひとつずつ代入していき、教師データと、重みベクトルを含んだ近似式の近似データを得る。
# 4. 3で得た全教師データと近似データの距離（誤差）の総和を取る。　距離（誤差）が許容範囲内であれば、近似式が得られる。
# 5. 距離（誤差）が大きかったため、重みベクトルを最急降下法を用いて更新する。
# -最急降下法-
# 5.1 誤差関数を、重みベクトルの要素一つ一つに対し偏微分し、学習率との積を元の重みベクトルの要素に足す。
# 更新する際、全要素は更新前の値を使う。
#
# 3-5を繰り返す。

MAX = 5
MIN = 0.1 
DEMENTION = 5
cap_dis = 100

def Function(x) : 
    return np.log(x)

def Dif_Function(x) : 
    return 1.0 / x

def Neighbor(x, w) : 
    X = np.array([np.zeros(len(w))])
    for value in range(0, len(w)) : 
        X[0][value] = x ** value

    # 独立変数ベクトルXと重みベクトルｗの内積
    return np.dot(X, w)


def DataSet(x, w) : 
    teach_data, test_data = [], []

    for value in range(0, len(x)) : 
        teach_data.append(Function(x[value]))
        test_data.append(Neighbor(x[value], w)[0])

    return test_data, teach_data

def CalcDistance(a, b) : 
    sum = 0

    for value in range(0, len(a)) : 
        sum = sum + (a[value] - b[value])

    return 0.5 * (sum ** 2)

def Diff(a, b) : 
    sum = 0

    for value in range(0, len(a)) : 
        sum = sum + (b[value] - a[value])

    return sum

def UpdateWeight(w, x, teach_data, learning_rate) : 
    updated = []
    test_data = []

    for value in range(0, len(x)) : 
        test_data.append(Neighbor(x[value], w))

    for value in range(0, len(w)) : 
        updated.append(w[value] - (learning_rate * Diff(teach_data, test_data)))

    return updated

def main() : 
    # 最小二乗法で多項式回帰をする。

    # 重みベクトル、教師データの設定
    x = np.arange(MIN, MAX, 0.1)
    w = np.random.rand(DEMENTION)
    test_data, teach_data = DataSet(x, w)

    print x
    print w
    print teach_data
    print test_data
