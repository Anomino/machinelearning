#!/bin/usr/env python3
# coding:UTF-8

import generate_prime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

DIM = [1,2,3,4,5,6,7,8,9]
LIMIT = 1000
SAVE_PASS = '../data/'

def LoadDataSet(limit = LIMIT) : 
    #datas = pd.read_csv('../data/primes.csv')
    primes = generate_prime.GeneratePrimeNumbers(limit = limit)

    #datas = pd.DataFrame([[np.arange(len(primes))], primes])
    
    return primes


# 最尤推定で、近似多項式を求める。
def ResolveByLikelihood(dataset, dim) : 
    teach = dataset[1]
    phi_mat = pd.DataFrame()

    for x in range(0, dim + 1) : 
        p = dataset[0] ** x
        p.name = "x ** {0}".format(x)
        phi_mat = pd.concat([phi_mat, p], axis = 1)

    tmp = np.linalg.inv(np.dot(phi_mat.T, phi_mat))
    ws = np.dot(np.dot(tmp, phi_mat.T), teach)

    def fit(x) : 
        y = 0.0
        for i, w in enumerate(ws) : 
            y += w * (x ** i)

        return y

    sigma = 0.0
    for index, line in dataset.iterrows() : 
        sigma += (fit(line[0]) - line[1]) ** 2

    sigma /= len(dataset[0])

    return (ws, fit, sigma)


def MaximumLogLikelihood(dataset, f) : 
    dev = 0.0
    n = float(len(dataset[0]))

    for index, line in dataset.iterrows() : 
        x, y = line[0], line[1]
        dev += (y - f(x)) ** 2

    err = dev * 0.5
    beta = n / dev
    lp = -beta * err + 0.5 * n * np.log(0.5 * beta / np.pi)

    return lp


def CreateFormula(w) : 
    formula = '{0} + '.format(w[0])
    for dim, w in enumerate(w) : 
        if dim != 0 : 
            formula += '{0}x^{1} +  '.format(w, dim)

    return formula

def Test() : 
    primes = LoadDataSet()
    weight, fit = ResolveByLikelihood(primes, DIM)

    #fig = plt.figure()
    
    plt.scatter(primes[0], primes[1], marker = 'o', color = 'blue')
    y = fit(primes[0])
    plt.scatter(primes[0], y, marker = 'o', color = 'red')

    result = pd.concat([primes[0], primes[1], y], axis = 1)
    plt.show()


def Main() : 
    teach_data = LoadDataSet()
    x_lim = len(teach_data[0])
    y_lim = max(teach_data[1])

    df_ws = pd.DataFrame()
    df = pd.DataFrame()
    train_mlh = []
    test_mlh = []

    result = pd.DataFrame(teach_data)
    #formula = pd.DataFrame()

    fig = plt.figure()
    for x, dim in enumerate(DIM) : 
        weight, fit, sigma = ResolveByLikelihood(teach_data, dim)
        df_ws = df_ws.append(pd.Series(weight, name = 'DIM = {0}'.format(dim)))
        fit_data = fit(teach_data[0])

        subprot = fig.add_subplot(3, 3, x + 1)
        subprot.set_xlim(-0.05, x_lim + 0.05)
        subprot.set_ylim(-5, y_lim + 10)
        subprot.set_title('Dim : {0}'.format(dim))

        subprot.plot(teach_data[0], teach_data[1], linestyle = '--', label = 'True', color = 'blue')
        subprot.plot(teach_data[0], fit_data, linestyle = '--', label = 'Approximation', color = 'red')

        subprot.legend(loc = 1)

        train_mlh.append(MaximumLogLikelihood(teach_data, fit))
        test_mlh.append(MaximumLogLikelihood(teach_data, fit))

        tmp = pd.DataFrame(fit_data, columns = ['Dim : {0}'.format(dim)])
        result = pd.concat([result, tmp], axis = 1)

        formula = CreateFormula(weight)
        print('{0}次近似式 : {1}'.format(dim, formula))
        #formula = pd.concat([formula, pd.DataFrame([CreateFormula(weight)], index = '{0}dims app formula'.format(dim))], axis = 0)

    df = pd.concat([df, pd.DataFrame(train_mlh, columns = ['Training set']),
        pd.DataFrame(test_mlh, columns = ['Test set'])], axis = 1)

    df.plot(title = 'Log likelihood for N = {0}'.format(len(teach_data[0])), grid = True, style = ['-', '--'])
    #result = pd.concat([result, formula], axis = 0)
    
    result.to_csv(SAVE_PASS + 'result.csv')
    fig.show()
    plt.show()

if __name__ == "__main__" : 
    #Test()
    Main()
