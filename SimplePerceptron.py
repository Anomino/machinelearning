#coding:UTF-8

# 単純パーセプトロンの動きの可視化

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

class Perceptron : 
    """
        パーセプトロンクラス
        トレーニングセットと、それに対する属性値とパラメータの学習率を与え、
        最急降下法で解を導く。

        トレーニングセットの形式は以下
        1行目　X軸データ
        2行目　y軸データ
    """
    def __init__(self, training, attribute, learning_rate, bottom_line = 0.1) : 
        self.learning_rate = learning_rate
    
        # トレーニングセットと、その属性値を一つのデータセットにする。
        unit = pd.DataFrame(np.ones(len(training.ix[0])))
        self.training = unit.T.append(training)
        self.attribute = attribute
        self.bottom_line = bottom_line

        # データの次元数
        self.data_dim = len(training.ix[0])

        # 行列それぞれのラベルの編集
        self.training.index = ["Bias", "X", "Y"]
        self.training.columns = range(0, len(self.training.ix[0]))

        # 重みベクトルの初期化
        self.weight = pd.DataFrame(np.ones(3))

    def SteepestDescentMethod(self) : 
        # 最急降下法で解を導く
        
        for a in range(10) : 
            # 誤差判定
            score = 0
            tmp = 0

            for value in range(self.data_dim) : 
                tmp += self.attribute[0][value] * np.dot(self.training[value], self.weight)
                print tmp

                if tmp <= 0 : 
                    # データが不正解である
                    gra_dif = self.attribute[0][value] * self.training[value]
                    score -= tmp


            if score < self.bottom_line : 
                # 正常に分類できている
                return self.weight

            else : 
                # 重みベクトルに誤り有り
                # 誤差修正
                self.weight -= self.learning_rate * gra_dif
                tmp = 0
            

def Sumple() : 
    # 定義域の設定
    x = np.linspace(-5, 5, 0.1)
    y = np.linspace(-5, 5, 0.1)

    # サンプルの設定
    t = pd.DataFrame([[-5, -8, 4], [5, 2, 9]])
    print t

    # プロットの設定
    plt.plot(t.ix[0], t.ix[1], "o")
    plt.xlim(-10, 10)
    plt.ylim(-10, 10)

    w = pd.DataFrame([-1,-1, 1])

    p = Perceptron(t, w, 0.4)

    print p.SteepestDescentMethod()


if __name__ == "__main__" : 
    Sumple()
