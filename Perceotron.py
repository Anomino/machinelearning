#!/usr/bin/env python3
# coding:UTF-8

"""
    パーセプトロンの学習規則
"""

import numpy as np

class Perceptron(object) : 
    def __init__(self, education = 0.01, n_iter = 10) : 
        self.education = education
        self.n_iter = n_iter

    def Fit(self, x_data, y_data) : 
        self.weight = np.zeros(1 + x_data.shape[1])
        self.errors = []

        for _ in range(self.n_iter) : 
            errors = 0

            for xi, target in zip(x_data, y_data) : 
                update = self.education * (target - self.Predict(xi))
                self.weight[1 :] += update * xi
                self.weight[0] += update

                errors += int(update != 0.0)

            self.errors.append(errors)

        return self

    def NetInput(self, x_data) : 
        return np.dot(x_data, self.weight[1 :]) + self.weight[0]

    def Predict(self, x_data) : 
        return np.where(self.NetInput(x_data) >= 0.0, 1, -1)
